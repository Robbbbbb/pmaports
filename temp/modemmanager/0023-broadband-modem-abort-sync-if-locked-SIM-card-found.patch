From f466eda93a2a28421557ec9a7f1bf5d0aec7dd00 Mon Sep 17 00:00:00 2001
From: Aleksander Morgado <aleksander@aleksander.es>
Date: Sun, 23 May 2021 14:44:44 +0200
Subject: [PATCH 23/39] broadband-modem: abort sync if locked SIM card found

We made sure that the after resume synchronization was only started on
modems that had been enabled (and so unlocked). If we detect a locked
SIM card during the sync operation, it's either because the SIM card
was swapped, or because the modem was fully shutdown during the
suspension (and so the SIM requires SIM-PIN again).

Either way, we cannot sync the state cleanly at this point, we must
trigger a full modem reprobe in order to move back the modem state to
Locked.
---
 src/mm-broadband-modem.c | 26 +++++++++++++++-----------
 1 file changed, 15 insertions(+), 11 deletions(-)

diff --git a/src/mm-broadband-modem.c b/src/mm-broadband-modem.c
index ac5fbb1c..d9a6d6ac 100644
--- a/src/mm-broadband-modem.c
+++ b/src/mm-broadband-modem.c
@@ -11404,22 +11404,26 @@ iface_modem_sync_ready (MMIfaceModem *self,
                         GAsyncResult *res,
                         GTask        *task)
 {
-    SyncingContext     *ctx;
-    MMModemLock         lock;
-    g_autoptr (GError)  error = NULL;
+    SyncingContext    *ctx;
+    MMModemLock        lock;
+    g_autoptr(GError)  error = NULL;
 
     ctx = g_task_get_task_data (task);
-    lock = mm_iface_modem_get_unlock_required (self);
 
-    if (!mm_iface_modem_sync_finish (self, res, &error)) {
-        mm_obj_warn (self, "synchronizing Modem interface failed: %s", error->message);
-    }
+    if (!mm_iface_modem_sync_finish (self, res, &error))
+        mm_obj_warn (self, "modem interface synchronization failed: %s", error->message);
 
-    /* SIM is locked, skip synchronization */
+    /* The synchronization logic only runs on modems that were enabled before
+     * the suspend/resume cycle, and therefore we should not get SIM-PIN locked
+     * at this point, unless the SIM was swapped. */
+    lock = mm_iface_modem_get_unlock_required (self);
     if (lock == MM_MODEM_LOCK_UNKNOWN || lock == MM_MODEM_LOCK_SIM_PIN || lock == MM_MODEM_LOCK_SIM_PUK) {
-        mm_obj_warn (self, "SIM is locked... Synchronization skipped");
-        ctx->step = SYNCING_STEP_LAST;
-        syncing_step (task);
+        /* Abort the sync() operation right away, and report a new SIM event that will
+         * disable the modem and trigger a full reprobe */
+        mm_obj_warn (self, "SIM is locked... synchronization aborted");
+        g_task_return_new_error (task, MM_CORE_ERROR, MM_CORE_ERROR_ABORTED,
+                                 "Locked SIM found during modem interface synchronization");
+        g_object_unref (task);
         return;
     }
 
-- 
2.31.1

