# Reference: <https://postmarketos.org/devicepkg>
pkgname=device-kobo-clara
pkgdesc="Kobo Clara HD"
pkgver=0.2
pkgrel=0
url="https://postmarketos.org"
license="MIT"
arch="armv7"
options="!check !archcheck"
depends="
	perl
	postmarketos-base
	u-boot-kobo-clara
	u-boot-tools
"
makedepends="devicepkg-dev"
install="$pkgname.post-install"
subpackages="
	$pkgname-kernel-downstream:kernel_downstream
	$pkgname-kernel-mainline:kernel_mainline
	$pkgname-nonfree-firmware:nonfree_firmware
"
source="
	deviceinfo
	extract-waveform.initd
	extract-waveform.pl
	uboot-script-downstream.cmd
	uboot-script-mainline.cmd
"

build() {
	devicepkg_build $startdir $pkgname
}

package() {
	devicepkg_package $startdir $pkgname
	install -Dm755 "$srcdir"/extract-waveform.pl "$pkgdir"/usr/sbin/extract-waveform.pl
	install -Dm755 "$srcdir"/extract-waveform.initd "$pkgdir"/etc/init.d/extract-waveform
}

kernel_downstream() {
	pkgdesc="Downstream kernel"
	depends="linux-kobo-clara"
	devicepkg_subpackage_kernel $startdir $pkgname $subpkgname

	mkimage -A arm -O linux -T script -n postmarketOS \
		-d "$srcdir"/uboot-script-downstream.cmd "$srcdir/boot-downstream.scr"
	install -Dm644 "$srcdir/boot-downstream.scr" "$subpkgdir/boot/boot.scr"
}

kernel_mainline() {
	pkgdesc="Close to mainline kernel"
	depends="linux-kobo-clara-mainline"
	devicepkg_subpackage_kernel $startdir $pkgname $subpkgname

	mkimage -A arm -O linux -T script -n postmarketOS \
		-d "$srcdir/uboot-script-mainline.cmd" "$srcdir/boot-mainline.scr"
	install -Dm644 "$srcdir/boot-mainline.scr" "$subpkgdir/boot/boot.scr"
}

nonfree_firmware() {
	pkgdesc="Kobo Clara HD firmware"
	depends="firmware-kobo-clara"
	mkdir "$subpkgdir"
}

sha512sums="
8ff8c8a4a1cd274a060413423dea7767e06f4ab2bf539bcb1a084e2d4cb488115947e553e86e0c4a20287ca156ce167329a2c2c462a66094f1bec6ddccc07276  deviceinfo
c68e4e41d73a1f9d08aeed95743d5a3ae6831d36d0259ba96ceca297479fef01b78c2b569c376c5b24b4ee24ea7d3cb44d6e97730f44d81fd67e9f0586c4911a  extract-waveform.initd
34475b314e45135c84e4abdc769d3aba9df56e37814c7f689d5e1aca5af060b2e028c01e871c3863534fede0be7c79aaebe0a1ff4f2bfab2ee974489119005cf  extract-waveform.pl
9f95496929f46de69afb6b25b1e3d099668e0f759eb1d36f76d84af14c8bd78cb739c410c58d7a31283782c6ff11999575c8c7ec22c11adec018b3026b290408  uboot-script-downstream.cmd
5152e007591e07c4772ecbce4e212ae6c1b65264a46234c11c425f91439171a6379727e344a5c9654da368baa61b863660bf781e5e211d813532d29d8c2e7fd9  uboot-script-mainline.cmd
"
