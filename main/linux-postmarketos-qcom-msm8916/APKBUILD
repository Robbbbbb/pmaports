# Maintainer: Minecrell <minecrell@minecrell.net>
# Kernel config based on: arch/arm64/configs/msm8916_defconfig

_flavor="postmarketos-qcom-msm8916"
pkgname=linux-$_flavor
pkgver=5.14_rc7
pkgrel=0
pkgdesc="Mainline kernel fork for Qualcomm MSM8916 devices"
arch="aarch64 armv7"
url="https://github.com/msm8916-mainline/linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native
	pmb:kconfigcheck-anbox pmb:kconfigcheck-containers pmb:kconfigcheck-nftables"
makedepends="bison findutils flex installkernel openssl-dev perl gmp-dev mpc1-dev mpfr-dev"

# Architecture
case "$CARCH" in
	aarch64) _carch="arm64" ;;
	arm*)    _carch="arm" ;;
esac

# Source
_tag=v${pkgver//_/-}-msm8916
source="
	$pkgname-$_tag.tar.gz::$url/archive/$_tag.tar.gz
	config-$_flavor.aarch64
	config-$_flavor.armv7
"
builddir="$srcdir/linux-${_tag#v}"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$CARCH" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION=$((pkgrel + 1 ))
}

package() {
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir"/usr/share/dtb
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release
}

sha512sums="
32049a56278777abefd9107cac1d0e36db99c1ee73e55ec5eac89047c0f53985ba9807944a624ceab1df15dac2ac01100519231a44c4d164a86ea278a3c9858c  linux-postmarketos-qcom-msm8916-v5.14-rc7-msm8916.tar.gz
fbecc9c7e609799fabeb44a0f28ec8ce076fa0daef11cc8b4aab69888aacea78f9f8a96c69d6ef83f9a13cea9c6f1dc1595389c922d4fa03fe73df9c65ae8585  config-postmarketos-qcom-msm8916.aarch64
2aaa4c8658df207f87413cdc45f862edd1c0432913fa7d011d8579d9c952c51f1fbe84c2a4a9736cbdc311082999315c2a9343ca1307ad722b1e6d964e85cd30  config-postmarketos-qcom-msm8916.armv7
"
